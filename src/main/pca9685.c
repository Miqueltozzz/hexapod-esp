#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "esp_system.h"
#include "m_utils.h"
#include "pca9685.h"
#include "pca_i2c.h"


// PCA9685 register addresses
#define MODE1 0x00
#define MODE2 0x01
#define LED0_ON_L 0x06
#define REGS_PER_OUTPUT 4
#define PRE_SCALE 0xfe

// PCA9685 configuration
#define PCA_ADDR_1 0x40  // Address of the first PCA module
#define PCA_ADDR_2 0x41  // Address of the second PCA module
#define PRESCALER_VALUE 131  // Defines the frequency of the output
/*
PCA9685 internal clock is 30 MHz, and servo control is 12 bit, so frequency can be calculated as follows:
frequency = 30 MHz / 2^12 / Prescaler_value
But in reality, the internal clock on PCA9685 sucks, and can be off by 10%, so each one must be manually calibrated...
Protip: If doing custom design, use external clock with a crystal to get rid of this nuisance...
*/

#define MIN_PULSE 0.55  // Pulse length for servo 0 deg angle
#define MAX_PULSE 2.5  // Pulse length for servo 180 deg angle
#define PULSE_INTERVAL 20.0  // Interval between pulses (in ms), 50 Hz -> 20 ms


uint16_t angle_to_pulse(float angle)
{
    /*
    Converts angle in radians to pulse length in milliseconds.
    (The servos are controlled by pulse length)
    Allowed range is between 0-180 degrees (0-pi radians)
    */
    float pulse_ms = angle / 3.14 * (MAX_PULSE - MIN_PULSE) + MIN_PULSE;
    uint16_t pulse_count = 4096.0 / PULSE_INTERVAL * pulse_ms;
    return pulse_count;
}

void pca9685_init_single(uint8_t address)
{
    /*
    Initializes a single PCA9685 PWM controller
    */
    i2c_write_uint8(address, MODE1, 0b00111111);   // setup mode 1 to sleep for prescaler update
    i2c_write_uint8(address, PRE_SCALE, PRESCALER_VALUE);
    i2c_write_uint8(address, MODE1, 0b00101111);  // back from sleep
	i2c_write_uint8(address, MODE2, 0b00000100);
    // Reset all outputs to zero
    for (int i=0; i<64; i++)
    {
        i2c_write_uint8(address, LED0_ON_L + i, 0);
    }
}

void pca9685_init()
{
    /*
    Initializes all PCA9685 PWM controllers and I2C
    */
    printf("PCA9685 init started\n");
    i2c_master_init();
    pca9685_init_single(PCA_ADDR_1);
    pca9685_init_single(PCA_ADDR_2);
    printf("PCA9685 init finished\n");
}

void pca9685_set_output(uint8_t channel, float angle)
{
    /*
    Sets output of a PCA9685 to a certain pulse width
    One PCA has 16 channels (0-15), if channel is between 16-31 switch over to second module.
    Angle is in radians.
    */
    uint8_t address = PCA_ADDR_1;
    uint16_t pulse = angle_to_pulse(angle);
    if (channel > 15)
    {
        address = PCA_ADDR_2;
        channel -= 16;
    }
    if (channel > 15)
    {
        printf("PCA9685 invalid channel!!! %d\n", channel);
    }
    else
    {
        /*
        Each output channel has 4 registers, 2 for on time, 2 for off time
        Assume that first 2 registers(=on time) are zeroes, save time by not setting them
        */
        // Todo: Limit pulse value to 12 bits
        i2c_write_uint16(address, LED0_ON_L + REGS_PER_OUTPUT*channel+2, pulse);
    }
}