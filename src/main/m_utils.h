#ifndef __mutils__
#define __mutils__

#define PI 3.141593

#include "esp_system.h"  // datatype definitions
#include "math.h"  // math funcs are needed almost everywhere...
#include "esp_timer.h"

void print_num(uint8_t num);
void print_int32(int32_t num);
float sq(float a);
float deg_to_rad(float deg);
float rad_to_deg(float rad);


#endif