#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"
#include "freertos/queue.h"
#include "vectors.h"

#define BLINK_GPIO 5

void vCommandTask(void *pvParameters)
{
    /*
    This task is a placeholder for now. Currently it blinks led and sends periodic coordinates to motion task.

    This task will handle reading commands from bluetooth / wifi / whatever,
    and then send target parameters for motion task.

    Target is to send direction and gait as a command, not leg coordinates.
    */

   QueueHandle_t cmd_queue;
   cmd_queue = (QueueHandle_t)pvParameters;
    Vector3 target_pos = {
        .x=0.0,
        .y=0.0,
        .z=0.0
    };
    printf("Command task started\n");
    gpio_reset_pin(BLINK_GPIO);
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

    for(;;)
    {
        gpio_set_level(BLINK_GPIO, 0);
        vTaskDelay(6000 / portTICK_PERIOD_MS);
        target_pos.x = 75.0;
        target_pos.y = 20.0;
        target_pos.z = 75.0;
        xQueueSendToBack(cmd_queue, &target_pos, 0);
        gpio_set_level(BLINK_GPIO, 1);
        vTaskDelay(6000 / portTICK_PERIOD_MS);
        target_pos.x = 75.0;
        target_pos.y = -20.0;
        target_pos.z = 75.0;
        xQueueSendToBack(cmd_queue, &target_pos, 0);
    }
    vTaskDelete(NULL);  // This should never happen, but in case, delete the task
}