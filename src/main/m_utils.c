#include "m_utils.h"


void print_uint8(uint8_t num)
{
    /*
    Prints one uint8 in hex, bin and dec formats
    */
    char hexnum[5];
    itoa(num, hexnum, 16);
    char binnum[5];
    itoa(num, binnum, 2);
    char decnum[5];
    itoa(num, decnum, 2);
    printf("uint8_t: 0x%s --- 0b%s --- 0d%s\n", hexnum, binnum, decnum);
}


void print_int32(int32_t num)
{
    /*
    Prints one int32 as hex
    */
    char hexnum[5];
    itoa(num, hexnum, 16);
    printf("int32: %s\n", hexnum);
}

float sq(float a)
{
    /*
    Calculate square of a number
    */
    return a * a;
}

float deg_to_rad(float deg)
{
    return deg / 180.0 * PI;
}

float rad_to_deg(float rad)
{
    return rad / PI * 180.0;
}