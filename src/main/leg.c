#include "esp_system.h"
#include "math.h"
#include "vectors.h"
#include "pca9685.h"
#include "leg.h"
#include "m_utils.h"

void print_leg_config(LegConfig config)
{
    printf("Servo config:\n");
    printf("coxalength: %f, femurlength: %f, tibialength: %f\n", 
    config.coxaLength, config.femurLength, config.tibiaLength);

    printf("gammaoffset: %f, alphaoffset: %f, betaoffset: %f\n", 
    rad_to_deg(config.gammaOffset), rad_to_deg(config.alphaOffset), rad_to_deg(config.betaOffset));

    printf("coxachannel: %d, femurchannel: %d, tibiachannel: %d\n", 
    config.coxaChannel, config.femurChannel, config.tibiaChannel);

    printf("Invertgamma: %d, invertalpha: %d, invertbeta: %d",
    config.invertGamma, config.invertAlpha, config.invertBeta);
}

void calc_leg_angles(LegConfig config, Vector3 target_position)
{
    /*
    Maths loosely based on https://oscarliang.com/inverse-kinematics-and-trigonometry-basics/

    The following may be hard to follow without the physical model to watch.

    Leg structure is basically:
    BODY#-#GAMMA(joint)#----COXA----#ALPHA(joint)#---FEMUR---#BETA(joint)#---TIBIA

    Gamma: first joint, rotates around z-axis. 0 angle is sticking straight away from body
    Alpha: Angle between femur and z-axis. 0 is sticking straight down, 180 straight up
    Beta: Angle between femur and tibia. 0 angle would tibia sticking back towards femur. 180 is away from femur.

    Coxa: Distance from gamma joint to alpha joint
    Femur: Distance from alpha to beta joint
    Tibia: Distance from beta joint to end of "leg"

    Todo: Handle coxa properly - current model ignores it's length
    */
    bool print_debug = false;
    float alpha = 0.0;
    float beta = 0.0;
    float gamma = 0.0;
    float l3 = 0.0;  // Leg length in XYZ from gamma(or alpha?) to tibia end
    float leg_length_xy = 0.0;  // Leg length in XY plane from gamma to tibia end

    // First calculate gamma. This is rather trivial, just imagine the leg structure from above and this is trivial trigonometry
    leg_length_xy = sqrt(sq(target_position.x) + sq(target_position.y));
    gamma = asin(target_position.y / leg_length_xy);

    // Then the mean part - calculating alpha and beta. Check the web page in function definition, hard to explain without images
    l3 = sqrt(sq(leg_length_xy) + sq(target_position.z));

    /*
    This has been optimized by precalculating partial values
    alpha = acos(target_position.z / l3) + acos((sq(config.tibiaLength)-sq(config.femurLength)-sq(l3))/(-2.0*config.femurLength*l3));
    beta = acos((sq(l3)-sq(config.femurLength)-sq(config.tibiaLength))/(-2.0*config.femurLength*config.tibiaLength));
    */
    
    alpha = acos(target_position.z / l3) + acos((config.precalc_1-sq(l3))/(-2.0*config.femurLength*l3));
    beta = acos((sq(l3)-config.precalc_2)* config.precalc_3);
    
    if(print_debug)
    {
        print_leg_config(config);
        printf("initials: alpha: %f, gamma: %f, beta: %f \n", 
            rad_to_deg(gamma), rad_to_deg(alpha), rad_to_deg(beta));
    }
    
    // Add offsets to joint angles
    if (config.invertGamma)
    {
        gamma = PI - gamma;
    }
    if (config.invertAlpha)
    {
        alpha = PI - alpha;
    }
    if (config.invertBeta)
    {
        beta = PI - beta;
    }

    gamma += config.gammaOffset;
    alpha += config.alphaOffset;
    beta += config.betaOffset;
    
    if (print_debug)
    {
        printf("finals: alpha: %f, gamma: %f, beta: %f \n", 
            rad_to_deg(gamma), rad_to_deg(alpha), rad_to_deg(beta));
    }
    // Todo: Speedup write by writing all channels in one transaction?
    pca9685_set_output(config.coxaChannel, gamma);
    pca9685_set_output(config.tibiaChannel, beta);
    pca9685_set_output(config.femurChannel, alpha);
}

void precalc_values(LegConfig* config)
{
    /*
    Pre-calculate some of the more constant parts in the kinematic model
    This has to be only once when initializing the legs.
    */
    config->precalc_1 = sq(config->tibiaLength)-sq(config->femurLength);
    config->precalc_2 = sq(config->femurLength)-sq(config->tibiaLength);
    config->precalc_3 = 1/(-2.0*config->femurLength*config->tibiaLength);
}