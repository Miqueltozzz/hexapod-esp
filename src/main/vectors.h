#ifndef __vectors__
#define __vectors__

typedef struct Vector3
{
    float x;
    float y;
    float z;
}Vector3;

typedef struct Vector2
{
    float x;
    float y;
}Vector2;

Vector3 vector3_sum(Vector3 vector_a, Vector3 vector_b);
Vector3 vector3_subtract(Vector3 vector_a, Vector3 vector_b);
Vector3 vector3_multiply(Vector3 vector_a, float multiplier);
Vector3 vector3_divide(Vector3 vector_a, float divisor);
Vector3 vector3_step_linear(Vector3 current_pos, Vector3 target_pos, float distance);
void vector3_print(Vector3 vect);

Vector2 vector2_sum(Vector2 vector_a, Vector2 vector_b);
Vector2 vector2_subtract(Vector2 vector_a, Vector2 vector_b);

#endif