#ifndef __leg__
#define __leg__

typedef struct LegConfig	// Joints etc. are explained in calc_leg_angles function
{
	float coxaLength;
	float femurLength;
	float tibiaLength;
	float gammaOffset;
	float alphaOffset;
	float betaOffset;
	uint8_t coxaChannel;
	uint8_t femurChannel;
	uint8_t tibiaChannel;
	bool invertGamma;
	bool invertAlpha;
	bool invertBeta;
	float precalc_1;
	float precalc_2;
	float precalc_3;
}LegConfig;

typedef struct LegState
{
	Vector3 pose;
	float phase;
}LegState;

typedef struct Leg
{
	LegConfig config;
	LegState state;
}Leg;

void calc_leg_angles(LegConfig config, Vector3 target_position);
void precalc_values(LegConfig* config);
uint16_t angle_to_pulse(float angle);

#endif