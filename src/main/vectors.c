#include "vectors.h"
#include "m_utils.h"
#include "math.h"

Vector3 vector3_sum(Vector3 vector_a, Vector3 vector_b)
{
    /*
    Calculate sum of two 3d vectors
    */
    Vector3 result;
    result.x = vector_a.x + vector_b.x;
    result.y = vector_a.y + vector_b.y;
    result.z = vector_a.z + vector_b.z;
    return result;
}

Vector3 vector3_subtract(Vector3 vector_a, Vector3 vector_b)
{
    /*
    Subtract vector b from vector a
    */
    Vector3 result;
    result.x = vector_a.x - vector_b.x;
    result.y = vector_a.y - vector_b.y;
    result.z = vector_a.z - vector_b.z;
    return result;
}

Vector3 vector3_multiply(Vector3 vector_a, float multiplier)
{
    /*
    Multiply 3d vector by a scalar
    */
    Vector3 result;
    result.x = vector_a.x * multiplier;
    result.y = vector_a.y * multiplier;
    result.z = vector_a.z * multiplier;
    return result;
}

Vector3 vector3_divide(Vector3 vector_a, float divisor)
{
    /*
    Divide 3d vector by a scalar.
    Returns zero vector is divisor is zero
    */
    if (divisor == 0.0)
    {
        Vector3 zero_vector = {.x=0.0, .y=0.0, .z=0.0};
        return zero_vector;
    }
    Vector3 result;
    result.x = vector_a.x / divisor;
    result.y = vector_a.y / divisor;
    result.z = vector_a.z / divisor;
    return result;
}

Vector3 vector3_step_linear(Vector3 current_pos, Vector3 target_pos, float distance)
{
    /*
    Calculate new position, that is certain distance from current position towards target position.
    */
    Vector3 delta_vect = vector3_subtract(target_pos, current_pos);
    float vect_length = sqrt(sq(delta_vect.x) + sq(delta_vect.y) + sq(delta_vect.z));
    if (abs(vect_length) < distance)  // Avoid later dividing by zero (vect_length is divisor later)
    {
        return target_pos;
    }
    float multiplier = distance / vect_length;
    delta_vect = vector3_multiply(delta_vect, multiplier);
    delta_vect = vector3_sum(current_pos, delta_vect);
    return delta_vect;
}

void vector3_print(Vector3 vect)
{
    /*
    Prints a 3d-vector as a decimal number. Includes line break.
    */
    printf("%f, %f, %f\n", vect.x, vect.y, vect.z);
}

Vector2 vector2_sum(Vector2 vector_a, Vector2 vector_b)
{
    /*
    Sums two 2d vectors
    */
    Vector2 result;
    result.x = vector_a.x + vector_b.x;
    result.y = vector_a.y + vector_b.y;
    return result;
}

Vector2 vector2_subtract(Vector2 vector_a, Vector2 vector_b)
{
    /*
    Subtract vector b from vector a
    */
    Vector2 result;
    result.x = vector_a.x - vector_b.x;
    result.y = vector_a.y - vector_b.y;
    return result;
}
