#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "vectors.h"
#include "leg.h"
#include <stdio.h>
#include <string.h>
#include "math.h"
#include "pca9685.h"
#include "m_utils.h"
#include "driver/uart.h"


// Indexes for the legs array
#define FRONT_LEFT 0
#define MID_LEFT 1
#define REAR_LEFT 2
#define FRONT_RIGHT 3
#define MID_RIGHT 4
#define REAR_RIGHT 5

Leg legs[6];



void init_leg_configs();
void leg_debugger();
void leg_motion(LegState* state);

void vMotionTask(void *pvParameters)
{
    /*
    This task handles leg kinematics using all 18 servos.
    
    This task also includes a debug mode, which can be used to manually control the legs.

    Currently this task gets coordinates for the command task, but goal is to get just
    direction and gait.
    */
   TickType_t xLastWakeTime;  // Holds timer value for periodic task delay
   xLastWakeTime = xTaskGetTickCount();
   QueueHandle_t cmd_queue;
   cmd_queue = (QueueHandle_t)pvParameters;

    Vector3 target_position = {
        .x = 75,
        .y = 0,
        .z = 75
    };
    bool debug_mode = false;

    init_leg_configs();
    pca9685_init(); // Initialize PWM modules

    // If not debugging, set legs to a known initial position
    if (!debug_mode)
    {
        for (int i=0; i<6; i++)
        {
            calc_leg_angles(legs[i].config, legs[i].state.pose);
        }
    }

    for(;;)
    {
        if (debug_mode)
        {
            leg_debugger();
        }
        else
        {
            for (int i=0; i<6; i++)
                {
                    leg_motion(&(legs[i].state));
                    calc_leg_angles(legs[i].config, legs[i].state.pose);
                }

            if (uxQueueMessagesWaiting(cmd_queue) > 0)
            {
                printf("Position cmd from queue: ");
                xQueueReceive(cmd_queue, &target_position, 0);
                vector3_print(target_position);
            }
        }
        vTaskDelayUntil( &xLastWakeTime, 20 / portTICK_PERIOD_MS );
    }
    vTaskDelete(NULL);  // The loop should never exit, but just in case...
}

void leg_debugger()
{
    /*
    Leg position debugging tool.
    Allows controlling single servo channels with angles (=used for figuring out leg configs)
    Allows controlling single servo positions in XYZ coordinates.
    Not very memory safe at the moment, but good enough for debugging the legs.
    */
    const int size = 256;
    char str[size]; // Full user input string
    char cmd[size]; // Parsed command
    int values[10] = {0};   // Parsed numbers
    char pos_cmd[] = "pos";
    
    if (fgets(str, size, stdin) != NULL)
    {
        fflush(stdin);
        printf("Received: %s \n", str);
        char delim[] = " ,:;";

        // Parse first value, should be text command
        char *ptr = strtok(str, delim);
        if (ptr == NULL)
        {
            return;
        }
        strcpy(cmd, ptr);
        printf("Cmd: %s\n", cmd);

        // Get the first expected numeric value
        int index = 0;
        ptr = strtok(NULL, delim);
        while (ptr != NULL) // Todo: make sure values array bounds are safe
        {
            values[index] = atoi(ptr);
            printf("Num: %d\n", values[index]);
            ptr = strtok(NULL, delim);
            if (ptr != NULL)
            {
                index += 1;
            }
        }
        if (strcmp(pos_cmd, cmd) == 0)
        {
            printf("Position command:\n");
            if (index != 3)
            {
                printf("Invalid index: %d\n", index);
                return;
            }
            Vector3 pos_vals;
            int servo_index = values[0];
            if ((servo_index < 0) || (servo_index > 5))
            {
                printf("Invalid servo channel: %d\n", servo_index);
                return;
            }
            pos_vals.x = values[1];
            pos_vals.y = values[2];
            pos_vals.z = values[3];
            vector3_print(pos_vals);
            calc_leg_angles(legs[servo_index].config, pos_vals);
        }
        else // Default choice: angle command
        {
            printf("Angle command:\n");
            if (index!=1)
            {
                printf("Invalid index: %d\n", index);
                return;
            }
            int channel = values[0];
            int angle = values[1];
            printf("Angle command: channel %d, angle: %d\n", channel, angle);
            pca9685_set_output(channel, deg_to_rad(angle));
        }
    }
}

void init_leg_configs()
{
    /*
    Configure all the legs.
    These need to be manually set for each leg.
    */

    LegConfig leg;

    Vector3 initial_pose = {.x=0, .y=75, .z=75};
    LegState state = {.pose = initial_pose, .phase = 0.0};

    // Common values for all legs
    leg.coxaLength = 17.0;
    leg.femurLength = 54.5;
    leg.tibiaLength = 88.5;

    // Common values for left side:
    leg.invertGamma = true;
    leg.invertAlpha = true;
    leg.invertBeta = false;

    // Front left
    leg.gammaOffset = deg_to_rad(-110.0);
    leg.alphaOffset = deg_to_rad(-5.0);
    leg.betaOffset = deg_to_rad(-75);
    leg.coxaChannel = 0;
    leg.femurChannel = 1;
    leg.tibiaChannel = 2;
    legs[0].config = leg;
    
    state.phase = 0.0;
    legs[0].state = state;

    // Mid left
    leg.gammaOffset = deg_to_rad(-110.0);
    leg.alphaOffset = deg_to_rad(-5.0);
    leg.betaOffset = deg_to_rad(-75);
    leg.coxaChannel = 4;
    leg.femurChannel = 5;
    leg.tibiaChannel = 6;
    legs[1].config = leg;

    state.phase = 0.5;
    legs[1].state = state;

    // Rear left
    leg.gammaOffset = deg_to_rad(-110.0);
    leg.alphaOffset = deg_to_rad(-5.0);
    leg.betaOffset = deg_to_rad(-75);
    leg.coxaChannel = 16;
    leg.femurChannel = 17;
    leg.tibiaChannel = 18;
    legs[2].config = leg;

    state.phase = 0.0;
    legs[2].state = state;

    // Common values for right side:
    leg.invertGamma = false;
    leg.invertAlpha = false;
    leg.invertBeta = true;

    // Front right
    leg.gammaOffset = deg_to_rad(70.0);
    leg.alphaOffset = deg_to_rad(-35.0);
    leg.betaOffset = deg_to_rad(25);
    leg.coxaChannel = 8;
    leg.femurChannel = 9;
    leg.tibiaChannel = 10;
    legs[3].config = leg;

    state.phase = 0.5;
    legs[3].state = state;

    // Mid right
    leg.gammaOffset = deg_to_rad(70.0);
    leg.alphaOffset = deg_to_rad(-35.0);
    leg.betaOffset = deg_to_rad(25);
    leg.coxaChannel = 12;
    leg.femurChannel = 13;
    leg.tibiaChannel = 14;
    legs[4].config = leg;

    state.phase = 0.0;
    legs[4].state = state;

    // Rear right
    leg.gammaOffset = deg_to_rad(70.0);
    leg.alphaOffset = deg_to_rad(-35.0);
    leg.betaOffset = deg_to_rad(25);
    leg.coxaChannel = 20;
    leg.femurChannel = 21;
    leg.tibiaChannel = 22;
    legs[5].config = leg;

    state.phase = 0.5;
    legs[5].state = state;

    // To reduce leg position calculation compute time, precalculate some values based on configs for each leg
    for (int i=0; i<6; i++)
    {
        precalc_values(&leg+i);
    }
}

void leg_motion(LegState* state)
{
    /*
    WIP:
    Advances led position for forward walk
    Leg phase advances 0.0...1.0 repeatedly
    At 0.0, leg starts touching ground
    at 0.5, raise leg up (maintain some y motion for smoothness??)
    at 0.6 start moving towards above start position
    at 0.9, start moving down (also already some y movement for smoothness??)

    Todo:
    Rework to work with speed, direction + rotation, get rid of manual step size calculation
    */

   Vector3 phase1 = {.x=75, .y=40, .z=75};
   Vector3 phase2 = {.x=75, .y=-40, .z=75};
   Vector3 phase3 = {.x=75, .y=-45, .z=50};
   Vector3 phase4 = {.x=75, .y=45, .z=50}; 
   state->phase += 0.01;
   if (state->phase > 1)
   {
       state->phase = 0.0;
   }

   if (state->phase < 0.5)
   {
       state->pose = vector3_step_linear(state->pose, phase2, 0.32);
   }
   else if (state->phase < 0.6)
   {
       state->pose = vector3_step_linear(state->pose, phase3, 1.08);
   }
   else if (state->phase < 0.9)
   {
       state->pose = vector3_step_linear(state->pose, phase4, 0.64);
   }
   else
   {
       state->pose = vector3_step_linear(state->pose, phase1, 1.08);
   }
   
}