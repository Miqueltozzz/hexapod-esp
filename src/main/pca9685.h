#ifndef __PCA9685__
#define __PCA9685__

void pca9685_init();
void pca9685_set_output(uint8_t channel, float angle);

#endif