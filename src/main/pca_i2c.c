#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "m_utils.h"
#include "pca_i2c.h"

// I2C definitions and config
// Some I2C stuff copied from i2c example https://gist.github.com/mws-rmain/2ba434cd2a3f32d6d343c1c60fbd65c8
#define I2C_SCL_IO				27	//19               /*!< gpio number for I2C master clock */
#define I2C_SDA_IO				26	//18               /*!< gpio number for I2C master data  */
#define I2C_FREQ_HZ				200000           /*!< I2C master clock frequency */
#define I2C_PORT_NUM			I2C_NUM_1        /*!< I2C port number for master dev */
#define I2C_TX_BUF_DISABLE  	0                /*!< I2C master do not need buffer */
#define I2C_RX_BUF_DISABLE  	0                /*!< I2C master do not need buffer */

#define WRITE_BIT                          I2C_MASTER_WRITE /*!< I2C master write */
#define READ_BIT                           I2C_MASTER_READ  /*!< I2C master read */
#define ACK_CHECK_EN                       0x1              /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS                      0x0              /*!< I2C master will not check ack from slave */
#define ACK_VAL                            0x0              /*!< I2C ack value */
#define NACK_VAL                           0x1              /*!< I2C nack value */


void i2c_master_init()
{
    int i2c_master_port = I2C_PORT_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_FREQ_HZ;
    esp_err_t ret = 0;
    ret = i2c_param_config(i2c_master_port, &conf);
    if (ret != ESP_OK)
    {
        printf("init: Error in i2c_param_config\n");
    }
    ret = i2c_set_data_mode(i2c_master_port, I2C_DATA_MODE_MSB_FIRST, I2C_DATA_MODE_MSB_FIRST);
    if (ret != ESP_OK)
        {
            printf("init: Error setting data mode\n");
        }
    ret = i2c_driver_install(i2c_master_port, conf.mode, I2C_RX_BUF_DISABLE, I2C_TX_BUF_DISABLE, 0);
    if (ret != ESP_OK)
    {
        printf("init: Error in i2c_driver_install\n");
    }
}


void i2c_write_uint8(uint8_t i2c_address, uint8_t register_address, uint8_t data)
{
    esp_err_t ret = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    // Send device address
    i2c_master_write_byte(cmd, ( i2c_address << 1 ) | WRITE_BIT, ACK_CHECK_EN);
    // Send register address
    i2c_master_write_byte(cmd, register_address, ACK_CHECK_EN);
    // Write data
    i2c_master_write_byte(cmd, data, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    // cmd_begin executes the previously queued commands?
    ret = i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
    if (ret != ESP_OK)
    {
        printf("I2C error when writing uint8\n");
    }
    i2c_cmd_link_delete(cmd);
}

void i2c_write_uint16(uint8_t i2c_address, uint8_t register_address, uint16_t data)
{
    esp_err_t ret = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    // Send device address
    i2c_master_write_byte(cmd, ( i2c_address << 1 ) | WRITE_BIT, ACK_CHECK_EN);
    // Send register address
    i2c_master_write_byte(cmd, register_address, ACK_CHECK_EN);
    // Write data
    i2c_master_write_byte(cmd, (uint8_t)data, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, (uint8_t)(data>>8), ACK_CHECK_EN);
    i2c_master_stop(cmd);
    // cmd_begin executes the previously queued commands?
    ret = i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
    if (ret != ESP_OK)
    {
        printf("I2C error when writing uint8\n");
    }
    i2c_cmd_link_delete(cmd);
}


uint8_t i2c_read_uint8(uint8_t i2c_address, uint8_t register_address)
{
    esp_err_t ret = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ret = i2c_master_start(cmd);
    // Send device address
    ret = i2c_master_write_byte(cmd, ( i2c_address << 1 ), ACK_CHECK_EN);
    // Send register address
    ret = i2c_master_write_byte(cmd, register_address, ACK_CHECK_EN);
    // Send repeated start
    ret = i2c_master_start(cmd);
    // Send device address again
    ret = i2c_master_write_byte(cmd, ( i2c_address << 1 ) | READ_BIT, ACK_CHECK_EN);
    uint8_t data = 0;
    uint8_t * data_ptr = &data;
    // Read data from slave
    ret = i2c_master_read_byte(cmd, data_ptr, NACK_VAL);
    ret = i2c_master_stop(cmd);
    // Execute the queued commands
    ret = i2c_master_cmd_begin(I2C_PORT_NUM, cmd, 1000 / portTICK_RATE_MS);
    if (ret != ESP_OK)
    {
        printf("I2C error when reading uint8\n");
    }
    i2c_cmd_link_delete(cmd);
    return data;
}