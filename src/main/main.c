#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include <stdio.h>
#include "sdkconfig.h"
#include "commander.h"
#include "motion.h"
#include "vectors.h"


void app_main()
{
    // Command queue will transfer the commands from command task to motion task
    QueueHandle_t command_queue;
    command_queue = xQueueCreate(5, sizeof(Vector3));

    xTaskCreate(&vCommandTask, "CmdTask", 4096, (void*)command_queue, 2, NULL);
    xTaskCreate(&vMotionTask, "MotionTask", 4096, (void*)command_queue, 1, NULL);
    
    for(;;)
    {
        // Main tasks just starts the other tasks, and is not needed after that
        printf("Suspending main task\n");
        vTaskSuspend(NULL);
    }
}