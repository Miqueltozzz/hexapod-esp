#ifndef __PCA_I2C__
#define __PCA_I2C__

void i2c_master_init();
void i2c_write_uint8(uint8_t i2c_address, uint8_t register_address, uint8_t data);
void i2c_write_uint16(uint8_t i2c_address, uint8_t register_address, uint16_t data);
uint8_t i2c_read_uint8(uint8_t i2c_address, uint8_t register_address);

#endif